Source: python-redis
Section: python
Priority: optional
Maintainer: Chris Lamb <lamby@debian.org>
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 python-all,
 python-setuptools,
 python3-all,
 python3-setuptools,
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/lamby/pkg-python-redis.git
Vcs-Browser: https://salsa.debian.org/lamby/pkg-python-redis
Homepage: https://github.com/andymccurdy/redis-py

Package: python-redis
Architecture: all
Depends:
 ${misc:Depends},
 ${python:Depends},
Suggests:
 python-hiredis,
Description: Persistent key-value database with network interface (Python library)
 Redis is a key-value database in a similar vein to memcache but the dataset
 is non-volatile. Redis additionally provides native support for atomically
 manipulating and querying data structures such as lists and sets.
 .
 The dataset is stored entirely in memory and periodically flushed to disk.
 .
 This package contains Python bindings to Redis.

Package: python3-redis
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python3-hiredis,
Description: Persistent key-value database with network interface (Python 3 library)
 Redis is a key-value database in a similar vein to memcache but the dataset
 is non-volatile. Redis additionally provides native support for atomically
 manipulating and querying data structures such as lists and sets.
 .
 The dataset is stored entirely in memory and periodically flushed to disk.
 .
 This package contains Python bindings to Redis.
 .
 This is the Python 3 version of the package.
